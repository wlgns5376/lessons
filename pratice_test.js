function solution(answers) {
    var answer = [];
    var soopoja_answers = [
        [1, 2, 3, 4, 5],
        [2, 1, 2, 3, 2, 4, 2, 5],
        [3, 3, 1, 1, 2, 2, 4, 4, 5, 5]
    ];
    var points = soopoja_answers.map((v,i) => [i+1, 0]);

    answers.forEach((v, i) => {
        soopoja_answers.forEach((ans, index) => {
            if (ans[i%ans.length] == v) {
                points[index][1]++;
            }
        })
    })

    points.sort((a, b) => b[1] - a[1]);

    var max_point = points[0][1];
    for (let point of points) {
        if (point[1] == max_point) {
            answer.push(point[0]);
        } else {
            break;
        }
    }

    return answer;
}

console.log(solution([1,2,3,4,5]));
console.log(solution([1,3,2,4,2]));