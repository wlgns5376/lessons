function solution(priorities, location) {
    var prite_list = [];
    // [B, C, D, A]
    var priorities_map = priorities.map((v, i) => {
        return [v, i];
    })

    while(priorities_map.length > 0) {
        var doc = priorities_map.shift();
        for (var i=0; i< priorities_map.length; i++) {
            if (priorities_map[i][0] > doc[0]) {
                priorities_map.push(doc);
                doc = null;
                break;
            }
        }

        if (doc) {
            prite_list.push(doc);
            if (doc[1] == location) {
                break;
            }
        }
    }

    return prite_list.length;
}

console.log(solution([2, 1, 3, 2], 2))
console.log(solution([1, 1, 9, 1, 1, 1]	, 0))
console.log(solution([2, 1, 2, 1, 2], 0))