/**
 * 1 - index 가 1의 index보다 크고 1보다 작거나 같은 값이 있는 index 
 * 2
 * 3 - 
 * 2
 * 3 
 */


function solution(prices) {
    var answer = [];
    var prices_length = prices.length;
    for (var i=0; i < prices_length; i++) {
        var index = 0;
        for (var j=i+1; j < prices_length; j++) {
            index++;
            if (prices[j] < prices[i]) {
                break;
            }
            
        }
        answer.push(index);
    }
    return answer
}

console.log(solution([1, 2, 3, 2, 3]))