
function solution(begin, target, words) {
    var answer = 0;
    
    answer = dfs(begin, target, words, [], answer);

    return answer;
}
function dfs(begin, target, words, visited, answer) {
    if (words.indexOf(target) == -1) {
        return 0;
    }
    console.log('s', begin, target)
    if (begin == target) {
        console.log('finish', answer)
        return answer;
    }

    for(var i=0; i<words.length; i++) {
        
        console.log(begin, words[i], target, visited[i])
        if (!visited[i]) {
            var word = words[i];
            if (other_count(begin, word) == 1) {
                visited[i] = true;
                answer = dfs(word, target, words, [...visited], answer+1);
            }
        }
    }

    return answer;
}

function other_count(begin, target) {
    var begin = begin.split('');
    var target = target.split('');
    var count = 0;
    for(var i=0; i<target.length; i++) {
        if(begin[i] != target[i]) { 
            count++;
        }
    }

    return count;
}

console.log(solution("hit", "cog", ["hot", "dot", "dog", "lot", "log", "cog"]));
//console.log(solution("hit", "cog", ["cog", "log", "lot", "dog", "dot", "hot"]));
// hot, dot
// hot, lot

// hot, dot, dog
// hot, dot, lot
// hot, lot, log

// hot, dot, dog, log
// hot, dot, dog, cog
// hot, dot, lot, log
// hot, lot, log, cog

// hot, dot, dog, log, cog
// hot, dot, dog, cog
// hot, dot, lot, log, cog
// hot, lot, log, cog
