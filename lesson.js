function solution(n) {
    var answer = 0;
    var numbers = [];
    for (var i = 2; i <= n; i++) {
        numbers[i] = i;
    }

    for (var i = 2; i <=n; i++) {
        if (numbers[i] == 0) continue;

        for (var j = i * 2; j <=n; j+=i) {
            numbers[j] = 0;
        }
    }

    for (var i = 2; i<=n; i++) {
        if (numbers[i] != 0) answer++;
    }

    return answer;
}

console.log(solution(1000000))