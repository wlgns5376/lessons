// 소수 판별
function primality(num) {
    for (var i=2; i*i<=num; i++) {
        if (num % i == 0) return false;
    }

    return true;
}

function solution(nums) {
    var answer = 0;
    var len = nums.length;
    for (var i=0; i<len; i++) {
        for (var j=i+1; j<len;j++) {
            for (var k=j+1; k<len; k++) {
                if (primality(nums[i] + nums[j] + nums[k])) {
                    answer++;
                }
            }
        }
    }
    return answer;
}
console.log(
    solution([1,2,3,4,5,6]), // 1
);